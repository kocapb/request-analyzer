<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200521105727 extends AbstractMigration
{
    /** @var string */
    protected $tableName = 'candidates';

    /**
     * Description for migration
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Candidates create table';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $table = $schema->createTable($this->tableName);
        $table->addColumn('id', Types::BIGINT, [
            'autoincrement' => true,
            'unsigned' => true,
        ])->setNotnull(true);
        $table->addColumn('email', Types::STRING, [
            'length' => 128,
        ])->setNotnull(false);
        $table->addColumn('name', Types::STRING, [
            'length' => 64,
        ])->setNotnull(false);
        $table->addColumn('surname', Types::STRING, [
            'length' => 64,
        ])->setNotnull(false);
        $table->addColumn('resume', Types::STRING, [
            'length' => 128,
        ])->setNotnull(false);
        $table->addColumn('uuid', Types::STRING, [
            'length' => 36,
        ])->setNotnull(false);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['email']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable($this->tableName);
    }
}
