<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200519131725 extends AbstractMigration
{
    /** @var string */
    protected $tableName = 'logs';

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Logs create table';
    }

    /**
     * @param Schema $schema
     * @throws Exception
     */
    public function up(Schema $schema): void
    {
        $table = $schema->createTable($this->tableName);
        $table->addColumn('id', Types::BIGINT, [
            'autoincrement' => true,
            'unsigned' => true,
        ])->setNotnull(true);
        $table->addColumn('email', Types::STRING, [
            'length' => 128,
            'default' => null,
        ])->setNotnull(false);
        $table->addColumn('ip', Types::STRING, [
            'length' => 15,
        ])->setNotnull(true);
        $table->addColumn('status', Types::BOOLEAN, [
            'default' => false,
        ]);
        $table->addColumn('uuid', Types::STRING, [
            'length' => 36,
        ])->setNotnull(true);
        $table->addColumn('description', Types::TEXT)->setNotnull(true);
        $table->addColumn('created_at', Types::DATETIME_MUTABLE)->setNotnull(false);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['uuid']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $schema->dropTable($this->tableName);
    }
}
