<?php


namespace App\Entities;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\Uuid;

/**
 * Class Log
 * @package App\Entities
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $surname
 * @Entity()
 * @Table(name="candidates")
 * @ORM\Id
 */
class Candidate extends Entity
{
    /**
     * @id
     * @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /**
     * @Column(type="string", name="email", length=128)
     */
    public $email;

    /**
     * @Column(type="string", name="name", length=64)
     */
    public $name;

    /**
     * @Column(type="string", name="surname", length=64)
     */
    public $surname;

    /**
     * @Column(type="string", name="resume", length=128)
     */
    public $resume;

    /**
     * @Column(type="string", name="uuid", length=36)
     */
    protected $uuid;

    /**
     * @param $value
     */
    public function setUuid($value)
    {
        $this->uuid = $value;
    }

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'name', 'surname', 'resume', 'uuid'
    ];

    /**
     * @return array
     */
    public function constraints(): array
    {
        return [
            'email' => [new Email(), new NotBlank()],
            'name' => [new NotBlank()],
            'surname' => [new NotBlank()],
            'resume' => [new NotBlank(), new Url()],
            'uuid' => [new Uuid(), new NotBlank()]
        ];
    }

}
