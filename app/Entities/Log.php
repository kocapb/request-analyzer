<?php


namespace App\Entities;

use Symfony\Component\Validator\Constraints\Email;
use DateTime;

/**
 * Class Log
 * @package App\Entities
 * @property int $id
 * @property string $email
 * @property string $ip
 * @property string $description
 * @property string $uuid
 * @Entity()
 * @Table(name="logs")
 * @ORM\Id
 */
class Log extends Entity
{
    /**
     * @id
     * @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /**
     * @Column(type="string", name="email", length=128)
     */
    public $email;

    /**
     * @Column(type="string", name="ip", length=15)
     */
    public $ip;

    /**
     * @Column(type="text", name="description")
     */
    public $description;

    /**
     * @Column(type="boolean", name="status")
     */
    protected $status = false;

    /**
     * @Column(type="datetime", name="created_at")
     */
    private $created_at;

    /**
     * @Column(type="string", name="uuid", length=36)
     */
    private $uuid;

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'status', 'description'
    ];

    /**
     * @var array
     */
    protected $cast = [
        'status' => 'boolval',
        'description' => 'json_encode'
    ];

    /**
     * Log constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->created_at = new DateTime("now");
        $this->ip = ip();
        $this->uuid = uuid_create(UUID_TYPE_RANDOM);
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param bool $value
     */
    public function setStatus(bool $value)
    {
        $this->status = $value;
    }

    /**
     * @return array
     */
    public function constraints(): array
    {
        return [
            'email' => [new Email()],
        ];
    }

}
