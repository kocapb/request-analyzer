<?php


namespace App\Traits;

use App\System\View\View;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * Class Responses
 * @package App\Traits
 */
trait Responses
{
    /** @var View */
    protected $view;

    /**
     * @return View
     */
    protected function getView(): View
    {
        return $this->view ?? new View();
    }

    /**
     * @param string $path
     * @param array $data
     * @param int $status
     * @return HttpResponse
     */
    public function response(string $path, array $data = [], int $status = HttpResponse::HTTP_OK): HttpResponse
    {
        return new HttpResponse($this->getView()->make($path, $data), $status);
    }

    /**
     * @param array $data
     * @return HttpResponse
     */
    public function accessDenied(array $data = [])
    {
        return $this->response('basic/forbidden', $data = ['title' => 'Доступ закрыт'], HttpResponse::HTTP_FORBIDDEN);
    }
}
