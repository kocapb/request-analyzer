<?php


namespace App\Logic\Analyzer\Cases;


use App\Logic\Analyzer\CaseAbstractFactory;
use App\System\Interfaces\IAnalyzer;
use Symfony\Component\HttpFoundation\Request;

class Body extends CaseAbstractFactory implements IAnalyzer
{
    /** @var array */
    protected $data = [];

    /**
     * Body constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = json_decode($request->getContent(), true);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'body';
    }

    /**
     * @return string
     */
    public function getCaseMessage(): string
    {
        return 'В теле запроса должно быть формате json формата { email: ваша почта}, у вас email - ' . $this->getEmail();
    }

    /**
     * @return bool
     */
    public function condition(): bool
    {
        return (filter_var($this->data['email'] ?? '', FILTER_VALIDATE_EMAIL));
    }

    protected function getEmail()
    {
        return $this->data['email'] ?? 'пусто';
    }

}
