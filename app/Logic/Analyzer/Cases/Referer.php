<?php


namespace App\Logic\Analyzer\Cases;

use App\Logic\Analyzer\CaseAbstractFactory;
use App\System\Interfaces\IAnalyzer;

class Referer extends CaseAbstractFactory implements IAnalyzer
{
    /** @var string */
    public const REFERER_URL = 'https://www.google.com';

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'referer';
    }

    /**
     * @return string
     */
    public function getCaseMessage(): string
    {
        return 'На эту страничку нужно прийти из Google.com';
    }

    public function condition(): bool
    {
        return (self::REFERER_URL === $this->request->headers->get('referer'));
    }

}
