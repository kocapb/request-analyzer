<?php


namespace App\Logic\Analyzer\Cases;


use App\Logic\Analyzer\CaseAbstractFactory;
use App\System\Interfaces\IAnalyzer;
use Symfony\Component\HttpFoundation\Request;

class Visit extends CaseAbstractFactory implements IAnalyzer
{
    /** @var int */
    public const COUNT_VISIT = 33;

    /**
     * Visit constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $visit = $this->getRequest()->cookies->get($this->getName()) ?? 0;
        setcookie($this->getName(), ++$visit);
        $this->getRequest()->cookies->set($this->getName(), $visit);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'visit';
    }

    /**
     * @return string
     */
    public function getCaseMessage(): string
    {
        return 'Эту страничку нужно посетить ровно ' . self::COUNT_VISIT . ' раза(ваше посещение: ' . $this->getRequest()->cookies->get($this->getName()) . ')';
    }

    /**
     * @return bool
     */
    public function condition(): bool
    {
        return ($this->getRequest()->cookies->get($this->getName()) == self::COUNT_VISIT);
    }

}
