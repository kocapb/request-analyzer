<?php

namespace App\Logic\Analyzer\Cases;

use App\Logic\Analyzer\CaseAbstractFactory;
use App\System\Interfaces\IAnalyzer;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Method
 * @package App\Logic\Analyzer\Cases
 */
class Method extends CaseAbstractFactory implements IAnalyzer
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'method';
    }

    /**
     * @return string
     */
    public function getCaseMessage(): string
    {
        return "Нужен PATCH(а у вас: {$this->getRequest()->getMethod()})";
    }

    /**
     * @return bool
     */
    public function condition(): bool
    {
        return $this->request->isMethod(Request::METHOD_PATCH);
    }
}
