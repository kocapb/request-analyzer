<?php


namespace App\Logic\Analyzer;

use Symfony\Component\HttpFoundation\Request;

abstract class CaseAbstractFactory
{
    /** @var Request */
    protected $request;

    /**
     * CaseAbstractFactory constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }
}
