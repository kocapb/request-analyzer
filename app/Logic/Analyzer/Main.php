<?php


namespace App\Logic\Analyzer;

use App\Logic\Analyzer\Cases\Body;
use App\Logic\Analyzer\Cases\Method;
use App\Logic\Analyzer\Cases\Referer;
use App\Logic\Analyzer\Cases\Visit;
use App\System\Interfaces\IAnalyzer;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Main
 * @package App\Logic\Analyzer
 */
class Main
{
    /** @var string */
    public const DONE = 'done';
    /** @var string */
    public const NOT_DONE = 'not_done';
    /** @var array */
    protected $cases = [];
    /** @var int */
    protected $counter = 0;
    /** @var array */
    protected $caseMessages = [];

    /**
     * Main constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->addCases([
            new Method($request),
            new Visit($request),
            new Referer($request),
            new Body($request),
        ]);
        $this->init();
    }

    /**
     * @param IAnalyzer[] $cases
     */
    public function addCases(array $cases)
    {
        $this->cases = $cases;
    }

    /**
     * Метод инициализации сообщения
     */
    public function init(): void
    {
        $messages = $this->getMessages();
        /** @var IAnalyzer $class */
        foreach ($this->getCases() as $class) {
            $this->caseMessages[$class->getName()] = [
                'title' => $class->getCaseMessage(),
                'body' => $class->condition() ? $this->done() : $messages[self::NOT_DONE],
            ];
        }
    }

    /**
     * @return array
     */
    public function getCaseMassages(): array
    {
        return $this->caseMessages;
    }

    /**
     * @return array
     */
    public function getCases(): array
    {
        return $this->cases;
    }

    /**
     * Метод получения базовых сообшений успеха-отказ
     *
     * @return array
     */
    public function getMessages(): array
    {
        return [
            self::DONE => 'Выполнено',
            self::NOT_DONE => 'Не выполнено'
        ];
    }

    /**
     * Метод успешного выполнения условия
     *
     * @return mixed
     */
    protected function done()
    {
        $this->increment();
        return $this->getMessages()[self::DONE];
    }

    /**
     * Метод увеличения счетчика на единицу
     */
    protected function increment(): void
    {
        $this->counter++;
    }

    /**
     * @return int
     */
    public function getCounter(): int
    {
        return $this->counter;
    }

    /**
     * Метод проверки выполнения всех кейсов
     *
     * @return bool
     */
    public function isValidCase(): bool
    {
        return (sizeof($this->getCases()) === $this->getCounter());
    }

    /**
     * @return array
     */
    public function getCaseMessages()
    {
        return $this->caseMessages;
    }
}
