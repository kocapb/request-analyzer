<?php

namespace App\System\View;

use App\System\Interfaces\IView;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;

/**
 * Class View
 * @package App\System\View
 */
class View implements IView
{
    /** @var PhpEngine */
    private $template;

    /**
     * View constructor.
     */
    public function __construct()
    {
        /** @var FilesystemLoader $filesystemLoader */
        $filesystemLoader = new FilesystemLoader(BASE_PATH . VIEW_PATH . '%name%.php');
        $this->setTemplate(new PhpEngine(new TemplateNameParser(), $filesystemLoader));
    }

    /**
     * @param string $path
     * @param array $data
     * @return false|mixed|string
     */
    public function make(string $path, array $data = []): string
    {
        return $this->getTemplate()->render($path, $data);
    }

    /**
     * @return PhpEngine
     */
    public function getTemplate(): PhpEngine
    {
        return $this->template;
    }

    /**
     * @param PhpEngine $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }
}