<?php


namespace App\System\Interfaces;


interface IAnalyzer
{
    /**
     * Метод получения названия условия
     *
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getCaseMessage(): string;

    /**
     * @return bool
     */
    public function condition(): bool;
}
