<?php

namespace App\System\Interfaces;

/**
 * Interface IConfig
 * @package App\System\Interfaces
 */
interface IConfig
{
    /**
     * @param string $file
     */
    public function add(string $file): void;

    /**
     * @param array $list
     */
    public function addList(array $list): void;

    /**
     * @param string $key
     */
    public function get(string $key);
}