<?php

namespace App\Http\Controller;

use App\System\Interfaces\IController;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Traits\Responses;

/**
 * Class Controller
 * @package App\Http\Controller
 */
abstract class Controller implements IController
{
    use Responses;

    /** @var ValidatorInterface */
    protected $validator;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->setValidator(Validation::createValidator());
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @param mixed $validator
     */
    public function setValidator($validator): void
    {
        $this->validator = $validator;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return manager();
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return app()->getRequest();
    }

    protected function redirect($url)
    {
        return redirect($url);
    }

}
