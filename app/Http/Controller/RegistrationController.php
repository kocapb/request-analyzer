<?php

namespace App\Http\Controller;

use App\Entities\Candidate;
use App\Entities\Log;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegistrationController
 * @package App\Http\Controller
 */
class RegistrationController extends Controller
{
    /**
     * @param $uuid
     * @return Response
     */
    public function getRegistrationAction($uuid)
    {
        /** @var Log $log */
        $log = $this->getSuccessLog($uuid);
        if (!$log) {
            return $this->accessDenied();
        }

        return $this->response('registration/contact', [
            'email' => $log->email,
            'uuid' => $uuid,
            'title' => 'Форма обратной связи'
        ]);
    }

    /**
     * @param $uuid
     * @return Response
     */
    public function postRegistrationAction($uuid)
    {
        /** @var Log $log */
        $log = $this->getSuccessLog($uuid);
        if (!$log) {
            return $this->accessDenied();
        }
        $data = $this->getRequest()->request->all();
        $data['uuid'] = $uuid;

        /** @var $errors */
        $errors = ($candidate = new Candidate())->fill($data)->validate();
        if (empty($errors)) {
            $candidate->save();
            return $this->redirect(router()->generate('registration_done'));
        }

        return $this->response('registration/contact', [
            'errors' => $errors,
            'candidate' => $candidate,
            'email' => $data['email'],
            'title' => 'Форма обратной связи'
        ]);
    }

    /**
     * @param $uuid
     * @return Response
     */
    public function doneAction($uuid)
    {
        /** @var Candidate $log */
        $candidate = $this->getEntityManager()->getRepository(Candidate::class)->findOneBy([
            'uuid' => $uuid
        ]);
        if (!$candidate) {
            return $this->accessDenied();
        }

        return $this->response('registration/done', [
            'resume' => $this->getView()->getTemplate()->escape($candidate->resume)
        ]);
    }

    /**
     * @param string $uuid
     * @return object|null|Log
     */
    protected function getSuccessLog(string $uuid)
    {
        return $this->getEntityManager()->getRepository(Log::class)->findOneBy([
            'status' => true,
            'uuid' => $uuid,
        ]);
    }
}
