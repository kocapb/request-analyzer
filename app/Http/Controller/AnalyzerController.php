<?php

namespace App\Http\Controller;

use App\Entities\Analyzer;
use App\Entities\Log;
use App\Logic\Analyzer\Main;
use Symfony\Component\HttpFoundation\Response;
use Exception;
use Symfony\Component\Routing\Router;

/**
 * Class AnalyzerController
 * @package App\Http\Controller
 */
class AnalyzerController extends Controller
{
    /**
     * @return Response
     * @throws Exception
     */
    public function indexAction()
    {
        $email = json_decode($this->getRequest()->getContent())->email ?? null;

        if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $log = $this->getEntityManager()->getRepository(Log::class)->findOneBy([
                'email' => $email,
                'status' => true,
            ]);
            if ($log) goto success;
        }

        $analyzer = new Main($this->getRequest());
        $log = new Log();
        $log->email = $email;
        $log->description = json_encode($analyzer->getCaseMessages());

        if ($analyzer->isValidCase()) {
            $log->setStatus(true);
            $log->save();
            success:
            return $this->response('analyzer/success', [
                'title' => 'Завершение первого этапа тестирования',
                'url' => router()->generate('registration_get_route', ['uuid' => $log->getUuid()], Router::ABSOLUTE_URL),
            ]);
        }

        $log->save();
        return $this->response('analyzer/index', [
            'title' => 'SearchBooster - тестовое задание',
            'messages' => $analyzer->getMessages(),
            'caseMessages' => $analyzer->getCaseMessages(),
        ]);
    }
}
