# request-analyzer
Реализация паттерна MVC с помощью библиотек PHP. Данное приложение реализовано в ознокомительных целях, чтобы работодателю было на что посмотреть.
## Выполнение теста для анализа согласно условию
<ul>
  <li>Нужен PATCH запрос</li>
  <li>Страницу нужно посетить ровно 33 раза</li>
  <li>На эту страничку нужно прийти из Google.com</li>
  <li>В теле запроса должно быть формате json формата { email: ваша почта}</li>    
  <li>Реализация должна быть легко расширяема</li>
</ul>
   
## Install 
<pre>
composer update
</pre>
Edit config/database.yaml file for your database configuration
<pre>
php console.php migrations:migrate
</pre>
