<html lang="en-US">
<head>
    <?= include_once BASE_PATH . VIEW_LAYOUT_PATH . 'head.php' ?>
    <title><?= $title ?></title>
</head>
<body>
<div role="main" class="container">
    <div>
        <h1>Здравствуйте! </h1>
        <p class="lead">Чтобы пройти 1й этап нужно выполнить ряд условий</p>

        <ul>
            <?php
            foreach ($caseMessages as $message) {
                $badge = ($message['body'] === $messages[\App\Logic\Analyzer\Main::DONE]) ? 'badge-success' : 'badge-danger';
                echo '<li>' . $message['title'] . '<span class="badge badge-pill ' . $badge . '">' . $message['body'] . '</span></li>';
            }
            ?>
        </ul>
    </div>
</div>
</body>
