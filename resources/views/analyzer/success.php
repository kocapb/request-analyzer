<html lang="en-US">
<head>
    <?= include_once BASE_PATH . VIEW_LAYOUT_PATH . 'head.php' ?>
    <title><?= $title ?></title>
</head>
<body>
<div role="main" class="container">
    <div>
        <h1>Поздравляем! Вы прошли первый этап тестирования.</h1>
        <p>Чтобы завершить данный уровень перейдите по ссылке:
            <a href="<?= $url ?>" title="Завершение первого этапа"><?= $url ?></a>
        </p>
    </div>
</div>
</body>
