<html lang="en-US">
<head>
    <?= include_once BASE_PATH . VIEW_LAYOUT_PATH . 'head.php' ?>
    <title><?= $title ?></title>
    <style>
        .form-label-group {
            padding-bottom: 15px;
        }
    </style>
</head>
<body>
<div role="main" class="container">
    <div>
        <p class="lead">Заполните форму обратной связилсь</p>
    </div>
    <div class="col-md-row">
        <form class="form-login" action="<?= router()->generate('registration_post_route', ['uuid' => $uuid]) ?>"
              method="post">
            <div class="form-label-group">
                <div class="col-md-6">
                    <label for="inputEmail"></label>
                    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Ваша почта"
                           value="<?= isset($candidate) ? $candidate->email : $email ?>" required autofocus>
                </div>
            </div>
            <div class="form-label-group">
                <div class="col-md-6">
                    <label for="inputName"></label>
                    <input name="name" type="text" id="inputName" class="form-control" placeholder="Ваше имя"
                           value="<?= isset($candidate) ?? $candidate->name ?>" required>
                </div>
            </div>
            <div class="form-label-group">
                <div class="col-md-6">
                    <label for="inputSurname"></label>
                    <input name="surname" type="text" id="inputSurname" class="form-control" placeholder="Ваше фамилию"
                           value="<?= isset($candidate) ?? $candidate->surname ?>" required>
                </div>
            </div>
            <div class="form-label-group">
                <div class="col-md-6">
                    <label for="inputResume"></label>
                    <input name="resume" type="text" id="inputResume" class="form-control"
                           placeholder="Ссылку на резюме" required>
                </div>
                <div class="invalid-feedback" <?= isset($errors['resume']) ? 'style="display:block"' : '' ?>><?= isset($errors['resume']) ? $errors['resume'] : '' ?></div>
            </div>
            <div>
                <div class="col-md-6">
                    <button class="btn btn-primary float-right" type="submit">Выполнить</button>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
