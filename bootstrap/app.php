<?php

use App\System\App;
use App\System\Config\Config;
use App\System\Database\Database;
use Doctrine\ORM\ORMException;

require_once __DIR__ . '/../config/app.php';

$config = new Config(CONFIG_PATH);
$config->addList([
    'database.yaml',
    'app.yaml',
]);

$app = App::getInstance(BASE_PATH);
$app->addContainer('config', $config);

if (config('system.orm')) {
    try {
        $app->addContainer('database', new Database(config('database')));
    } catch (ORMException $e) {
    }
}
return $app;
