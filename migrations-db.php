<?php
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/app.php';

use App\System\Config\Config;

$config = new Config(CONFIG_PATH);
$config->addList([
    'database.yaml',
]);

return $config->get('database');
